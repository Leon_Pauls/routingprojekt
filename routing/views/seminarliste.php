<?php
require_once(ROOT_PATH . '/lib/db/connect.php');
include(ROOT_PATH . '/lib/db/seminar.php');

$page = isset($_GET['page']) && is_numeric($_GET['page']) ? $_GET['page'] : 1;

if (!isset($_SESSION['pagination_results'])) {
    $_SESSION['pagination_results'] = 10;
}
findAllSeminar();

$limit = $_SESSION['pagination_results'];
$calc_page = ($page - 1) * $limit;
?>
<h1 class="titel"><?php echo $head_titel; ?></h1>
<?php /* include "filter/search.php" */ ?>
<form method="post">
    <label>
        <input type="text" name="search" Placeholder="search seminar" required />
    </label>
    <input type="submit" value="search" onclick="findAllSeminar();" />
</form>
<table class="sortable">
    <tr>
        <th>Thema</th>
        <th>Seminar</th>
        <th>Dozent</th>
        <th>Preis</th>
    </tr>
    <?php include(ROOT_PATH . '/lib/elements_widgets/table/pagination/pagination_table.php') ?>
</table>
<?php include(ROOT_PATH . '/lib/elements_widgets/table/pagination/pagination_logic.php') ?>
<?php $_SESSION['Pagination_page'] = $page ?>
<br>
<?php include(ROOT_PATH . '/lib/elements_widgets/table/pagination/page_results/page_results.php') ?>
<div class="über">GFU Seminare</div>
<div class="normal">Hier könnte etwas über die Seite stehen.<br>Oder auch einfach etwas fülltext,<br>welcher unteranderem nur für den google algoritmus da ist<br>und kaum bedeutung an das Thema hat,<br> sollte er überhaupt irgendetwas am Thema zu tun haben.<br>Allerdings heißt dies noch lange nicht,<br>dass dieser Text gelesen werden soll.<br><br>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</div>
<script src="/routing/js/sorttable.js"></script>