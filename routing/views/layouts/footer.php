<footer class="footer">
    <div class="normal">
        <ul class="pagination">
            <li class="page">
                <?php if (!isset($_SESSION['Pagination_page'])) : ?>
                    <a href="?route=seminarliste&page=1">Zu den Seminaren</a>
                <?php else : ?>
                    <a href="?route=seminarliste&page=<?php echo $_SESSION['Pagination_page']; ?>">Zu den Seminaren</a>
                <?php endif; ?>
            </li>
        </ul>
        <div class="line"></div>
        <ul class="footer2">
            <li>23 <span class="footer3">Schulungsräume</span></li>
            <li>100+ <span class="footer3">Dozenten</span></li>
            <li>1.389 <span class="footer3">Seminarthemen</span></li>
            <li>22.263 <span class="footer3">Durchgeführte Seminare</span></li>
            <li>8.839 <span class="footer3">Kunden</span></li>
            <li>84.702 <span class="footer3">Teilnehmer</span></li>
            <li>Noch Fragen? <span class="footer3">Besuchen Sie unseren </span><a class="footer-text" href="/pagination/supporter_Seite/support.php">Support</a></li>
        </ul>
    </div>
</footer>