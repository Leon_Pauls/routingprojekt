<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/routing/css/style.css" rel="stylesheet">
    <title>Document</title>
</head>

<body>
    <?php include(ROOT_PATH . '/views/layouts/logo.php'); ?>
    <div class="text-box">
        <?php include(ROOT_PATH . '/lib/router.php'); ?>
    </div>
    <?php include(ROOT_PATH . '/views/layouts/footer.php'); ?>
</body>

</html>