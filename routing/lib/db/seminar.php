<?php

require_once(ROOT_PATH . '/lib/db/connect.php');

function search_seminar()
{
    if (isset($_POST['search'])) {
        $thema = "WHERE seminar_thema ";
        $thema_ok = true;
    } else {
        $thema_ok = false;
    }
    if (isset($_POST['search'])) {
        $search = "like '" . $_POST['search'] . "%'";
        $search_ok = true;
    } else {
        $search_ok = false;
    }

    if ($thema_ok == true && $search_ok == true) {
        $seminar_search = $thema . $search;
    } else if ($thema_ok == false && $search_ok == false) {
        $seminar_search = "";
    }
    return $seminar_search;
}

function findAllSeminar($offset = 0, $limit = 10)
{
    $limit = $_SESSION['pagination_results'];
    $mysqli = getConnection();
    $seminar_search = search_seminar();

    $stmt = $mysqli->prepare("SELECT * FROM seminartable $seminar_search LIMIT ?,?");
    $stmt->bind_param('ii', $offset, $limit);
    $stmt->execute();
    $result = $stmt->get_result();
    $seminarinfo = array();
    while ($row = $result->fetch_assoc()) {
        $seminarinfo[] = $row;
    }
    return $seminarinfo;
}

function totalSeminare()
{
    $mysqli = getConnection();
    $total_seminare = $mysqli->query('SELECT * FROM seminartable')->num_rows;
    $_SESSION['rows'] = $total_seminare;
    return $total_seminare;
}
