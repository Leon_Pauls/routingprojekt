<?php
$route = isset($_GET['route']);

if ($route) {
    switch ($_GET['route']) {
        case 'seminarliste':
            $head_titel = "Seminare im Überblick";
            include "views/seminarliste.php";
            break;
        case 'seminar':
            include "views/seminar.php";
            break;
        case 'buchen':
            include "views/buchen.php";
            break;
        default:
            $head_titel = '404 Seite nicht gefunden';
            include "views/404.php";
            break;
    }
}

if (!isset($_GET['route'])) {
    include "views/default.php";
}
