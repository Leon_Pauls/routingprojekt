<?php include(ROOT_PATH . '/config/pagination_includes.php') ?>
<?php if ($pagnation_pages_amount > 0) : ?>
    <ul class="pagination">
        <?php if ($page > 1) : ?>
            <li class="prev"><a href="<?php echo $current_link ?><?php echo $page - 1 ?>&item_per_page=10">Prev</a></li>
        <?php endif; ?>

        <?php if ($page > 3) : ?>
            <li class="start"><a href="<?php echo $current_link ?>1">1</a></li>
            <li class="dots">...</li>
        <?php endif; ?>

        <?php if ($page - 2 > 0) : ?><?php echo $current_pagination_minus_2 ?><?php endif; ?>
        <?php if ($page - 1 > 0) : ?><?php echo $current_pagination_minus_1 ?><?php endif; ?>

        <li class="currentpage"><a href="<?php echo $current_link ?><?php echo $page ?>"><?php echo $page ?></a></li>

        <?php if ($page + 1 < $pagnation_pages_amount + 1) : ?><?php echo $current_pagination_plus_1 ?><?php endif; ?>
        <?php if ($page + 2 < $pagnation_pages_amount + 1) : ?><?php echo $current_pagination_plus_2 ?><?php endif; ?>

        <?php if ($page < $pagnation_pages_amount - 2) : ?>
            <li class="dots">...</li>
            <li class="end"><a href="<?php echo $current_link ?><?php echo $pagnation_pages_amount ?>"><?php echo $pagnation_pages_amount ?></a></li>
        <?php endif; ?>

        <?php if ($page < $pagnation_pages_amount) : ?>
            <li class="next"><a href="<?php echo $current_link ?><?php echo $page + 1 ?>">Next</a></li>
        <?php endif; ?>
    </ul>
<?php endif; ?>