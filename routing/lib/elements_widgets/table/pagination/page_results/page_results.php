<ul class="pagination">
    <li class="page">
        <?php if ($_SESSION['pagination_results'] < totalSeminare()) : ?>
            <a href="/routing/lib/elements_widgets/table/pagination/page_results/page_results_max.php">↓↓</a>
            <a href="/routing/lib/elements_widgets/table/pagination/page_results/page_results_up.php"><?php echo $_SESSION['pagination_results'] + 1; ?> ↓</a>
        <?php endif ?>
        <?php if ($_SESSION['pagination_results'] > 1) : ?>
            <a href="/routing/lib/elements_widgets/table/pagination/page_results/page_results_down.php"><?php echo $_SESSION['pagination_results'] - 1; ?> ↑</a>
            <a href="/routing/lib/elements_widgets/table/pagination/page_results/page_results_min.php">↑↑</a>
        <?php endif ?>
    </li>
</ul>