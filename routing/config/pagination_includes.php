<?php
$pagnation_pages_amount = ceil(totalSeminare() / $limit);

$current_link = "?route=seminarliste&page=";

$page_minus_2 = $page - 2;
$page_minus_1 = $page - 1;
$page_plus_1 = $page + 1;
$page_plus_2 = $page + 2;

$current_pagination_minus_2 = "<li class='page'><a href='$current_link$page_minus_2'>$page_minus_2</a></li>";
$current_pagination_minus_1 = "<li class='page'><a href='$current_link$page_minus_1'>$page_minus_1</a></li>";
$current_pagination_plus_1 = "<li class='page'><a href='$current_link$page_plus_1'>$page_plus_1</a></li>";
$current_pagination_plus_2 = "<li class='page'><a href='$current_link$page_plus_2'>$page_plus_2</a></li>";
